const notemap = {"C":0, "C#":1, "D":2, "D#":3, "E":4, "F":5, "F#":6, "G":7, "G#":8, "A":9, "A#":10, "B":11};
const numbermap = {0:"C", 1:"C#", 2:"D", 3:"D#", 4:"E", 5:"F", 6:"F#", 7:"G", 8:"G#", 9:"A", 10:"A#", 11:"B"};
var btn, output, input;

function validate(input){
  var list = input.split(" ");
  for(item of list){
    if(notemap[item.toUpperCase()] == undefined) return false;
  }
  return true;
}

function notesToNumbers(notes){
  var numbers = [];
  for(note of notes.split(" ")){
     numbers.push(notemap[note.toUpperCase()]);
  }
  return numbers;
}

function generateNumberMatrix(numbers){
  var matrix = [];
  for(let i = 0; i < numbers.length; i++){
    matrix.push([]);
  }
  for(let i = 0; i < numbers.length; i++){
    matrix[i][0] = numbers[i];
  }

  //TODO
  for(let i = 0; i < numbers.length-1; i++){
    let diff = matrix[0][i] + (matrix[i][0] - matrix[i+1][0]);
    matrix[0][i+1] = (matrix[0][i] + (matrix[i][0] - matrix[i+1][0])+12)%12;
  }

  for(let i = 1; i < numbers.length; i++){
    for(let j = 1; j < numbers.length; j++){
      matrix[i][j] = ((matrix[i-1][j] - 1)+12)%12;
    }
  }
  return matrix;
}

function matrixToString(matrix){
  var s = '<table class="table table-bordered"><tbody>';

  //top labels
  s += "<tr><td></td>";
  for(var i = 0; i < matrix[0].length; i++){
    s += "<td>I" + matrix[0][i] + "</td>";
  }
  s += "<td></td></tr>";

  //each row 
  for(var i = 0; i < matrix.length; i++){
    s += "<tr>"
    s += "<td>P" + matrix[i][0] + "</td>";
    //each column
    for(var j = 0; j < matrix.length; j++){
      s += "<td>" + numbermap[matrix[i][j]] + "</td>"
    }
    s += "<td>R" + matrix[i][matrix[i].length-1]+ "</td>";
    s += "</tr>";
  }

  //bottom labels
  s += "<tr><td></td>";
  for(var i = 0; i < matrix[matrix.length-1].length; i++){
    s += "<td>RI" + matrix[matrix.length-1][i] + "</td>";
  }
  s += "<td></td></tr>";

  s += "</tbody></table>";
  return s;
}

function generate(){
  if(validate(input.value.trim())){
    output.innerHTML = matrixToString(generateNumberMatrix(notesToNumbers(input.value.trim())));
  }else{
    output.innerHTML = "Error parsing notes..."
  }
}

window.onload = function() {
  btn = document.getElementById('notes-button');
  output = document.getElementById('notes-output');
  input = document.getElementById('notes-input');

  input.addEventListener("input", function(){generate()});
  btn.addEventListener("click", function(){generate()});
};
